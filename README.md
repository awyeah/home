# AWYeah Website

- create a new post: `hugo new posts/foo.md`
- run a local server: `hugo serve -D` (`-D` shows drafted pages)

## TODO

- [ ] License - Necessary for a blog? What to use?
- [ ] Setup environments
